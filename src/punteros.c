#include <stdio.h>

#define SIZE 10

typedef struct data{
    int a;
    char *b;
} tba;

void mostrarBytes (void *valor, unsigned int tam){

    printf("%p\t", valor);
    for (int i = 0; i< tam; i++){
        printf("%02x ", *(unsigned char *)(valor +i) );
    }
    printf("\n");
}

int main (){

    long array[SIZE] = {0};

    for (int i = 0 ; i< SIZE ; i++){
        scanf("%ld", array+i);
    }

    // mostrar bytes del arreglo de enteros.
    printf("mostrar bytes del arreglo de enteros: \n\n");
    for (int i=0; i< SIZE ; i++){
        mostrarBytes(array+i ,sizeof( *(array+i) ) );
    }

    // mostrar bytes de la estructura.
    printf("\nmostrar bytes del arreglo de estructura.\n\n"); 
    tba structure;
    int num= 1234;
    structure.a= 1234;
    structure.b= (char *) &num;

    mostrarBytes(& structure ,sizeof( structure ) );

    return 0;
}